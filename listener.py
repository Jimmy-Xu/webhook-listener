import os
from sys import platform as _platform
from flask import Flask
from flask import request
import json
import sys
import subprocess
app = Flask(__name__)

USER_AGENT="Bitbucket-Webhooks/2.0"
X_HOOK_UUID="8315ad4d-2b9c-4fee-8143-4e6637453247"
X_EVENT_KEY="repo:push"
REPO = "getdvm/hyper-web-admin"
BRANCH = "master"
WORKDIR= "/opt/hyper-web-admin"

# check for ngrok subdomain
ngrok = ''
if 'NGROK_SUBDOMAIN' in os.environ:
    ngrok = os.environ['NGROK_SUBDOMAIN']

def displayIntro():
    if ngrok:
        print 'You can access this webhook publicly via at http://%s.ngrok.io/webhook\
        You can access ngrok\'s web interface via http://localhost:4040' % ngrok
    else:
        print 'Webhook server online! Go to http://localhost:5500'

def displayHTML(request):
    if ngrok:
        return 'Webhook server online! Go to <a href="https://bitbucket.com">Bitbucket</a> to configure your repository webhook for <a href="http://%s.ngrok.io/webhook">http://%s.ngrok.io/webhook</a> <br />\
            You can access ngrok\'s web interface via <a href="http://localhost:4040">http://localhost:4040</a>' % (ngrok,ngrok)
    else:
        return 'Webhook server online! Go to <a href="https://bitbucket.com">Bitbucket</a> to configure your repository webhook for <a href="%s/webhook">%s/webhook</a>' % (request.url_root,request.url_root)

@app.route('/', methods=['GET'])
def index():
    return displayHTML(request)

@app.route('/webhook', methods=['GET', 'POST'])
def tracking():
    if request.method == 'POST':
        data = request.get_json()
        commit_author = data['actor']['username']
        commit_hash = data['push']['changes'][0]['new']['target']['hash'][:7]
        commit_url = data['push']['changes'][0]['new']['target']['links']['html']['href']
        branch = data['push']['changes'][0]['new']['name']
        repo = data['repository']['full_name']

        # Show notification if operating system is OS X
        if _platform == "darwin":
            from pync import Notifier
            Notifier.notify('%s committed %s\nClick to view in Bitbucket' % (commit_author, commit_hash), title='Webhook received!', open=commit_url)
        else:
	        #print json.dumps(data)
            print '\n-------------------------------------------------------------------'
            print 'Webhook received! %s committed %s' % (commit_author, commit_hash)
            print 'repo[%s], branch[%s]' % (repo, branch)
            h_user_agent = request.headers.get('User-Agent')
            h_x_hook_uuid = request.headers.get("X_Hook_UUID")
            h_x_event_key = request.headers.get('X-Event-Key')
            h_x_attempt_number = request.headers.get('X-Attempt-Number')
            if h_user_agent == USER_AGENT and h_x_event_key == X_EVENT_KEY and h_x_hook_uuid == X_HOOK_UUID \
                and repo == REPO and branch == BRANCH and int(h_x_attempt_number) > 0 :
		        print 'valid hook, start pull code..'
		        base_dir = os.path.split( os.path.realpath( sys.argv[0] ) )[0]
		        git_ssh_key = '%s/.ssh/getdvm/deploy.pem' % ( os.environ['HOME'] )
		        cmd = '%s/../../script/git.sh -i %s git fetch' % ( base_dir, git_ssh_key )
		        if exec_cmd(cmd):
		            cmd = 'git checkout -f %s && git reset --hard origin/%s' %( BRANCH, BRANCH )
		            if exec_cmd(cmd):
		                print 'finish pull hyper-web-admin'
		            else:
		                print 'pull hyper-web-admin failed'
        return 'OK'
    else:
        return displayHTML(request)

def exec_cmd(cmd):
    print '> execute cmd: [%s]' % cmd
    p = subprocess.Popen(cmd, shell=True, cwd=WORKDIR, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    retcode = p.wait()
    if retcode == 0:
        print 'succeed:)'
        print p.stdout.read()
        return True
    else:
        print 'failed:('
        print p.stderr.read()
        return False


if __name__ == '__main__':
    displayIntro()
    app.run(host='0.0.0.0', port=5500, debug=True)
